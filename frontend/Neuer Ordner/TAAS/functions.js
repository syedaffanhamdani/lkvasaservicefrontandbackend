//Testdaten
var testTable = [['Test1', 'Test2', 'Test3'], ['Test4', 'Test5', 'Test6']];

function onLogin() {
	/*
	var elementsShown = document.getElementsByClassName('show');
	console.log(elementsShown);
	console.log(elementsShown.length);
	if(elementsShown && elementsShown.length != 0) {
		for(var i=0;i<elementsShown.length;i++) {
			console.log(i);
			elementsShown[i].classList.remove('show');
			elementsShown[i].classList.add('hidden');
		}		
	}
	*/

	switch(document.getElementById('username').value) {
		case 'Shipper':
			document.getElementById('shipperView').classList.remove('hidden');
			document.getElementById('shipperView').classList.add('show');
			console.log('Test');
			break;
		case 'Forwarder':
			document.getElementById('forwarderView').classList.remove('hidden');
			document.getElementById('forwarderView').classList.add('show');
			break;
		case 'Carrier':
			document.getElementById('carrierView').classList.remove('hidden');
			document.getElementById('carrierView').classList.add('show');
			break;
		case 'Driver':
			document.getElementById('driverView').classList.remove('hidden');
			document.getElementById('driverView').classList.add('show');
			break;
	}
}

function createTable(columnNames, data) {
	document.write('<table class="table table-bordered table-striped">');
	
	document.write('<thead>');
	for(var i=0;i<columnNames.length;++i) {
		document.write('<th>'+columnNames[i]+'</th>');
	}

	document.write('</thead>');
	
	document.write('<tbody>');
	data.forEach(function(row) {
		document.write('<tr>');
		row.forEach(function(element) {
			document.write('<td>'+element+'</td>');
		});
		document.write('</tr>');
	});
	document.write('</tbody>');
	document.write('</table>');
}

  function uploadFoto() {
	var imgFile = document.getElementById('submitfile');
	if (imgFile.files && imgFile.files[0]) {
		var width;
		var height;
		var fileSize;
		var reader = new FileReader();
		reader.onload = function(event) {
			var dataUri = event.target.result,
			img = document.createElement("img");
			img.src = dataUri;
			width = img.width;
			height = img.height;
			fileSize = imgFile.files[0].size;
			alert(width);
			alert(height);
			alert(fileSize);
	   };
	   reader.onerror = function(event) {
		   console.error("File could not be read! Code " + event.target.error.code);
	   };
	   reader.readAsDataURL(imgFile.files[0]);
	}
 }
 
 function getStyleForGPX_Plugin(element) {
	 console.log( element.style.width);
	 console.log( element.style.height);
	 
	 element.style.width = element.parent.style.width;
	 element.style.height = element.parent.style.height;
 }
 
 function insertNewCargo(formElement) {
	 console.log(formElement);
 }
 /*
 function GET_REQUEST() {
	$.get("localhost:9000/", function(data, status) {
		return data;
    });
 }
 
 function POST_RESPONSE() {
    $.post("localhost:9000", {
		
    },
    function(data, status) {
        return data;
    });
 }
 */
 function setTableVisible() {
	 console.log('Start');
	 var elements = document.getElementById('shipperOverview').getElementsByTagName('tr');
	  elements[2].style.display = 'contents';
 }
 
 
var my3words='';

// this function makes call to what3words api, called only when location coordinates are recieved form the browser
function sendRequest(){
var url = "https://api.what3words.com/v2/reverse";
var params = "coords="+currentCoordinates.lon+","+currentCoordinates.lat+"&display=full&format=json&key=X1EW2KKE";
var http = new XMLHttpRequest();

http.open("GET", url+"?"+params, true);
http.onreadystatechange = function()
{
    if(http.readyState == 4 && http.status == 200) {
        //alert(http.responseText);
        var obj = JSON.parse(http.responseText);
        my3words=obj.words;
		document.getElementById('tw_value').value = my3words;
    }
}
http.send(null);
}

//get location coordinates from browser
var currentCoordinates ={lat: "", lon: ""};
 navigator.geolocation.getCurrentPosition(function showLocation( position ) {
   var latitude = position.coords.latitude;
   var longitude = position.coords.longitude;
   currentCoordinates.lat = position.coords.latitude;;
//   console.log(currentCoordinates);
   currentCoordinates.lon = position.coords.longitude;
   sendRequest();
//   console.log(currentCoordinates);

//send request now
}, function errorHandler(){});
