﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class TruckAgent : Agent
{	
	[SerializeField]
	private StationInfo currentStation;
	[SerializeField]
	private StationInfo homeStation;
	[SerializeField]
	private bool startWithTrailer;
	[SerializeField]
	private int startTrailerIdx;
	[SerializeField]
	private bool hasTrailer;
	[SerializeField]
	private int thisTrucksTrailerIdx;

	private static int[] maxTrailerStepsCluster;
	private static int[] maxTrailerStepsRegular;
	public static int[] trailerSteps;
	public static int[] trailerDistanceToTarget;
	public static int[] lastTrailerDistanceToTarget;
	private int emptySteps = 0;
	private StationInfo[] availableStations;
	public bool needsToSleep = false;
	private bool truckAvailableAtCurrentStation;

	private bool trailerJustReceived = false;

	private void Start()
	{
		ResetAllShit();
	}

	public override List<float> CollectState()
	{
		List<float> state = new List<float>();

		state.Add(trailerDistanceToTarget[0]);
		state.Add(trailerDistanceToTarget[1]);
		state.Add(emptySteps);
		state.Add(trailerSteps[0]);
		state.Add(trailerSteps[1]);

		return state;
	}

	private void ResetAllShit()
	{
		maxTrailerStepsCluster = new int[2] { 4, 3 };
		maxTrailerStepsRegular = new int[2] { 7, 5 };
		trailerSteps = new int[2] {0, 0};

		trailerJustReceived = false;
		if (startWithTrailer)
		{
			hasTrailer = true;
			thisTrucksTrailerIdx = startTrailerIdx;

			trailerDistanceToTarget = new int[2];
			trailerDistanceToTarget[thisTrucksTrailerIdx] = homeStation.GetComponent<StationInfo>().distanceToTarget[thisTrucksTrailerIdx];

			lastTrailerDistanceToTarget = new int[2];
			lastTrailerDistanceToTarget[thisTrucksTrailerIdx] = trailerDistanceToTarget[thisTrucksTrailerIdx];

			Trailer[] trailers = FindObjectsOfType<Trailer>();
			for (int i = 0; i < trailers.Length; i++)
			{
				if (trailers[i].idx == thisTrucksTrailerIdx)
				{
					trailers[i].transform.SetParent(this.transform.GetChild(0), false);
					break;
				}
			}
		}
		else
		{
			hasTrailer = false;
		}

		currentStation = homeStation;
		transform.SetParent(currentStation.transform.GetChild(0), false);

		emptySteps = 0;
		needsToSleep = false;
	}

	public override void AgentReset()
	{
		ResetAllShit();
	}

	public override void AgentStep(float[] action)
	{
		// Action 0: Move [0;4] (0: stay, 1-4: move to station 1-4)
		// Action 1: Pass trailer [0;1] (0: keep, 1: pass if possible)

		currentStation = GetComponentInParent<StationInfo>();
		
		if (currentStation == homeStation)
		{
			availableStations = currentStation.nextAvailableStations;
		}
		else
		{
			availableStations = new StationInfo[1] { homeStation };
		}

		MoveToNextStation((int)action[0]);

		if (hasTrailer)
		{
			trailerDistanceToTarget[thisTrucksTrailerIdx] = currentStation.distanceToTarget[thisTrucksTrailerIdx];
			if (trailerDistanceToTarget[thisTrucksTrailerIdx] == 0)
			{
				reward = 1;
				done = true;
				FindObjectOfType<Academy>().AcademyReset();
				return;
			}

			if (trailerDistanceToTarget[thisTrucksTrailerIdx] < lastTrailerDistanceToTarget[thisTrucksTrailerIdx])
			{
				reward += 0.05f;
			}
			if (trailerDistanceToTarget[thisTrucksTrailerIdx] > lastTrailerDistanceToTarget[thisTrucksTrailerIdx])
			{
				reward -= 0.1f;
			}

			if (trailerSteps[thisTrucksTrailerIdx] > maxTrailerStepsCluster[thisTrucksTrailerIdx])
			{
				if (trailerSteps[thisTrucksTrailerIdx] < maxTrailerStepsRegular[thisTrucksTrailerIdx])
				{
					reward -= 0.1f;
				}
				else
				{
					reward -= 0.15f;
				}
			}
			
			lastTrailerDistanceToTarget = trailerDistanceToTarget;
		}

		if (!trailerJustReceived)
			PassTrailer((int)action[1]);

		trailerJustReceived = false;

		if ( (trailerSteps[0] > 7) || (trailerSteps[1] > 5) )
		{
			reward = -1;
			done = true;
			FindObjectOfType<Academy>().AcademyReset();
			return;
		}
	}

	public void MoveToNextStation(int nr)
	{
		if (nr < 0)
			nr = -nr;

		if (needsToSleep || (nr == 0) || (nr > availableStations.Length))
		{
			needsToSleep = false;
			return;
		}

		if (!hasTrailer)
		{
			emptySteps++;
			reward -= 0.035f;
		}

		currentStation = availableStations[nr-1];
		needsToSleep = true;

		if (hasTrailer)
		{
			trailerSteps[thisTrucksTrailerIdx]++;
		}

		transform.SetParent(currentStation.transform.GetChild(0), false);
		this.transform.localPosition = new Vector3(1.51f, -12.1f, 0);
		int truckCntOnStation = currentStation.transform.GetChild(0).childCount;
		if (truckCntOnStation > 1)
		{
			if (this.transform == currentStation.transform.GetChild(0).GetChild(1))
			{
				this.transform.Translate(new Vector3(truckCntOnStation*5, -truckCntOnStation*5, -0.1f));
			}
		}
	}

	private void PassTrailer(int pass)
	{
		if (pass < 0)
			pass = -pass;

		if ( hasTrailer && (pass > 0) )
		{
			Transform truckSpot = currentStation.transform.GetChild(0);
			int totalTrucksAtStation = truckSpot.childCount;
			List<TruckAgent> freeTrucksAtStation = new List<TruckAgent>();
			if (totalTrucksAtStation > 1)        // more than this truck standing here?
			{
				for (int i = 0; i < totalTrucksAtStation; i++)
				{
					TruckAgent truck = truckSpot.GetChild(i).GetComponent<TruckAgent>();
					if ( (truck != this) && !truck.needsToSleep && !truck.hasTrailer )
					{
						freeTrucksAtStation.Add(truck);
					}
				}
			}

			// Pass trailer
			if ( (pass <= freeTrucksAtStation.Count) && (freeTrucksAtStation.Count > 1) )
			{
				Debug.Log("Pass: " + pass.ToString() + " | " + "Count: " + freeTrucksAtStation.Count.ToString());
				if (GetComponentInChildren<Trailer>() != null)
				{
					GetComponentInChildren<Trailer>().transform.SetParent(freeTrucksAtStation[pass-1].transform.GetChild(0), false);
				}

				hasTrailer = false;
				freeTrucksAtStation[pass-1].hasTrailer = true;
				freeTrucksAtStation[pass-1].trailerJustReceived = true;
				freeTrucksAtStation[pass-1].thisTrucksTrailerIdx = thisTrucksTrailerIdx;

			}
		}
	}
}
