﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class TruckAcademy : Academy
{
	[SerializeField]
	private Text text;

	public override void AcademyReset()
	{
		TruckAgent[] agents = FindObjectsOfType<TruckAgent>();
		foreach (TruckAgent agent in agents)
		{
			agent.AgentReset();
		}
	}

	public override void AcademyStep()
	{
		text.text = "Steps: " + currentStep.ToString() + " | Episodes: " + episodeCount.ToString();
		text.text += "\nTrailer steps: " + TruckAgent.trailerSteps[0].ToString() + " | " + TruckAgent.trailerSteps[1].ToString();
		text.text += "\nTrailer dist: " + TruckAgent.trailerDistanceToTarget[0].ToString() + " | " + TruckAgent.trailerDistanceToTarget[1].ToString();
		text.text += "\nTrailer lastdist: " + TruckAgent.lastTrailerDistanceToTarget[0].ToString() + " | " + TruckAgent.lastTrailerDistanceToTarget[1].ToString();
	}
}
